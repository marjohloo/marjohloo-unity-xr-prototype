# marjohloo-unity-xr-prototype #

## Versions ##

Built on Unity 2019.3.13f1 using Oculus Rift.

## Assets ##

**[Standard Assets (v1.1.5) - Unity Technologies (Unity Asset Store)](https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-for-unity-2017-3-32351)**

Only `Standard Assets/Prototyping` folder.

## Packages ##

**[ProGrids (preview.6 - 3.0.3) - Package Manager (Advanced > Show preview packages)](https://docs.unity3d.com/Packages/com.unity.progrids@3.0/manual/index.html)**

**[XR Interaction Toolkit (preview - 0.9.4) - Package Manager (Advanced > Show preview packages)](https://blogs.unity3d.com/2019/12/17/xr-interaction-toolkit-preview-package-is-here/)**

**[XR Plugin Management (3.2.10) - Package Manager](https://docs.unity3d.com/Packages/com.unity.xr.management@3.2/manual/index.html)** 

## Setup ##

**[XR Interaction Toolkit (YouTube - VR with Andrew)](https://youtu.be/ndwJHpxd9Mo)**

1. Install *XR Interaction Toolkit* package.
2. Right click in scene hierarchy then *XR >  Room-Scale XR Rig* automatically adds *XR Interaction Manager*.
3. Install *XR Plugin Management* package.
4. *Edit > Project Settings... > XR Plug-in Management* check the required plug-in providers to install. 
5. Add sphere primitives as children of controllers (scaled to **0.1**).
6. On *Main Camera* reduce *Clipping Planes > Near* to **0.1**.

## Locomotion ##

**[XR Toolkit Movement (YouTube - VR with Andrew)](https://www.youtube.com/playlist?list=PLmc6GPFDyfw87eECUxsoysoIz82VifRwt)**

**[Gravity with character controller (Unity Answers - aldonaletto)](https://answers.unity.com/questions/334708/gravity-with-character-controller.html)**

1. On *XR Rig* add *Character Controller* component. 
    1. Set *Center > Y* to **1**. 
    2. Set *Radius* to **0.3**.
2. On *XR Rig* add *Locomotion System* component.
    1. Add *XR Rig* to *XR Rig*. 
3. On *XR Rig* add *Assets > MJL > Scripts > XrMovementProvider* script.
    1. Add *XR Rig* to *System*.
    2. Add *LeftHand Controller* to *Controller XZ*.
    3. Add *RightHand Controller* to *Controller Y*.
    4. Optionally add *Left Sphere* to *Object Grounded* (to indicate grounded state).
    5. Optionally add *Right Sphere* to *Object Gravity* (to indicate gravity state).
    6. Optionally add materials to *materialTrue* and *materialFalse*.

This provides the following controls:

**Controller XZ**

* *Joystick Click:* Toggle run on and off (also affect up/down flying speed), *Can Run* must be **true**.
* *Joystick Movement:* Move on XZ plane.

**Controller Y**

* *Joystick Click:* Toggle fly on and off, *Can Fly* must be **true**.
* *Joystick X-Axis:* Rotate around Y axis.
* *Joystick Y-Axis:* Move up and down on world Y-Axis, if in fly mode.
* *Primary Button:* Jump if grounded, *Can Jump* must be **true**.

## Disclaimers ##

Whilst I'm an embedded C programmer for my day job I've only been learning Unity for 7 weeks and XR for a couple of days. I'm just watching and reading stuff on the web to figure things out, so these uses might be completely inappropriate. 

## License ##

This code is released under the MIT License, see [LICENSE.txt](LICENSE.txt) for details.