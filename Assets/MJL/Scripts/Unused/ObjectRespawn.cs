﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRespawn : MonoBehaviour
{
    private Vector3 positionStart;
//    private Vector3 rotationStart;

    // Start is called before the first frame update
    void Start()
    {
        positionStart = transform.position;
//        rotationStart = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -5)
        {
//            transform.rotation = rotationStart;
            transform.position = positionStart;
        }
    }
}
