﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class StateFlyText : MonoBehaviour
{
    private TextMeshProUGUI tmpText = null;

    // Start is called before the first frame update
    void Start()
    {
        tmpText = gameObject.GetComponent<TextMeshProUGUI>();
    }

    public void StateFlyUpdate(bool value)
    {
        // Run state ? 
        if (tmpText != null)
        {
            if (value) tmpText.text = "Flying";
            else tmpText.text = "Grounded";
        }
    }
}
