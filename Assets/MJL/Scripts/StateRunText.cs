﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class StateRunText : MonoBehaviour
{
    private TextMeshProUGUI tmpText = null;

    // Start is called before the first frame update
    void Start()
    {
        tmpText = gameObject.GetComponent<TextMeshProUGUI>();
    }

    public void StateRunUpdate(bool value)
    {
        // Run state ? 
        if (tmpText != null)
        {
            if (value) tmpText.text = "Run";
            else tmpText.text = "Walk";
        }
    }
}
