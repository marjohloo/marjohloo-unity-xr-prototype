﻿// Unity XR Movement Provider Script
//
// Setup:        https://www.youtube.com/watch?v=ndwJHpxd9Mo
// Movement:     https://www.youtube.com/playlist?list=PLmc6GPFDyfw87eECUxsoysoIz82VifRwt
// Gravity/Jump: https://answers.unity.com/questions/334708/gravity-with-character-controller.html
//
// ==============================================================================
//
// MIT License
//
// Copyright(c) 2020 Martin Looker
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class XrMovementProvider : LocomotionProvider
{
    [Header("XR Controllers")]
    public XRController controllerXZ;
    public XRController controllerY;
    [Header("Speed")]
    public float speedWalk = 3.0f;
    public float speedRun  = 6.0f;
    public float speedJump = 6.0f;
    [Header("Rotate")]
    public float turnThreshold = 0.7f;
    public float turnTimeout = 0.15f;
    public float turnDegrees = 45;
    [Header("Options")]
    public bool canRun = true;
    public bool canJump = true;
    public bool canFly = true;
    [Header("Events")]
    public EventBool eventBoolStateRun;
    public EventBool eventBoolStateFly;

    // General private
    private CharacterController characterController = null;
    private GameObject head = null;
    private Vector3 vectorMove;
    // XZ variables private
    private bool stateRun = false;
    private bool primary2DAxisClickXZ = false;
    // Y variables private
    private bool stateFly = false;
    private bool primaryButtonY = false;
    private bool primary2DAxisClickY = false;
    private int turnState = 0;
    private float turnTimer = 0.0f;
    private float speedY = 0.0f;

    protected override void Awake()
    {
        // Get character controller component
        characterController = GetComponent<CharacterController>();
        // Get head object
        head = GetComponent<XRRig>().cameraGameObject;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Make sure events are initialized
        if (eventBoolStateRun == null) eventBoolStateRun = new EventBool();
        if (eventBoolStateFly == null) eventBoolStateFly = new EventBool();
        // Position the character controller
        PositionController();
        // Make sure states are set
        SetMovementStateRun(stateRun);
        SetMovementStateFly(stateFly);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Zero movement at start of update
        vectorMove = Vector3.zero;
        // Position the character controller
        PositionController();
        // Timer running - update it
        if (turnTimer > 0.0f) turnTimer -= Time.deltaTime;
        // Grounded ?
        if (characterController.isGrounded)
        {
            // Zero speedY
            speedY = 0;
        }
        // Update movement based upon controller input
        CheckForInput();
        // Flying ?
        if (stateFly)
        {
            // Zero speed Y
            speedY = 0;
        }
        // No gravity ? 
        else
        {
            // Apply gravity
            ApplyGravity();
        }
        // Move 
        characterController.Move(vectorMove * Time.deltaTime);
    }

    private void PositionController()
    {
        // Get the head in local, playspace ground
        float headHeight = Mathf.Clamp(head.transform.localPosition.y, 1, 2);
        characterController.height = headHeight;

        // Cut in half, add skin
        Vector3 newCenter = Vector3.zero;
        newCenter.y = characterController.height / 2;
        newCenter.y += characterController.skinWidth;

        // Move capsule in local space as well
        newCenter.x = head.transform.localPosition.x;
        newCenter.z = head.transform.localPosition.z;

        // Apply to character controller
        characterController.center = newCenter;
    }

    private void CheckForInput()
    {
        if(controllerXZ.enableInputActions)
        {
            CheckForMovementXZ(controllerXZ.inputDevice);
        }
        if (controllerY.enableInputActions)
        {
            CheckForMovementY(controllerY.inputDevice);
        }
    }

    private void CheckForMovementXZ(InputDevice device)
    {
        // Allowed to run ?
        if (canRun)
        {
            // Joystick click?
            if (device.TryGetFeatureValue(CommonUsages.primary2DAxisClick, out bool click))
            {
                // Changed ?
                if (primary2DAxisClickXZ != click)
                {
                    // Note new value 
                    primary2DAxisClickXZ = click;
                    // Clicked ? 
                    if (primary2DAxisClickXZ)
                    {
                        // Toggle applyRun
                        SetMovementStateRun(!stateRun);
                    }
                }
            }
        }
        // Joystick position ?
        if(device.TryGetFeatureValue(CommonUsages.primary2DAxis, out Vector2 position))
        {
            // Perform move
            StartMoveXZ(position);
        }
    }

    private void StartMoveXZ(Vector2 position)
    {
        // Apply the touch position to the heads forward vector
        Vector3 direction = new Vector3(position.x, 0, position.y);
        Vector3 headRotation = new Vector3(0, head.transform.eulerAngles.y, 0);
        // Rotate the input direction by the horizontal head rotation
        direction = Quaternion.Euler(headRotation) * direction;
        // Running ?
        if (stateRun)
        {
            // Apply run
            vectorMove = direction * speedRun;
        }
        // Walking ?
        else
        {
            // Apply walk
            vectorMove = direction * speedWalk;
        }
    }

    private void CheckForMovementY(InputDevice device)
    {
        // Allowed to fly ?
        if (canFly)
        {
            // Joystick click?
            if (device.TryGetFeatureValue(CommonUsages.primary2DAxisClick, out bool click))
            {
                // Changed ?
                if (primary2DAxisClickY != click)
                {
                    // Note new value 
                    primary2DAxisClickY = click;
                    // Clicked ? 
                    if (primary2DAxisClickY)
                    {
                        // Toggle applyFly
                        SetMovementStateFly(!stateFly);
                    }
                }
            }
        }

        if (device.TryGetFeatureValue(CommonUsages.primary2DAxis, out Vector2 position))
        {
            StartMoveY(position);
        }

        if (device.TryGetFeatureValue(CommonUsages.primaryButton, out bool button))
        {
            StartJumpY(button);
        }

    }

    private void StartMoveY(Vector2 position)
    {
        int state = 0;

        // Figure out new turn state
        if (position.x > turnThreshold) state = 1;
        else if (position.x < -turnThreshold) state = -1;
        // Turn state changed ? 
        if (turnState != state)
        {
            // Note new state
            turnState = state;
            // Returned to idle position ?
            if (turnState == 0)
            {
                // Timer not running - start it
                if (turnTimer <= 0.0f) turnTimer = turnTimeout;
            }
            // Want to turn and allowed ?
            else if (turnTimer <= 0.0f)
            {
                var xrRig = system.xrRig;

                // Get XR Rig
                if (xrRig != null)
                {
                    // Rotate XR Rig
                    xrRig.RotateAroundCameraUsingRigUp(turnState * turnDegrees);
                }
            }
        }

        // Flying ? 
        if (stateFly)
        {
            // Apply Y axis position to the direction
            Vector3 direction = new Vector3(0, position.y, 0);
            // Running ?
            if (stateRun)
            {
                // Apply run
                vectorMove.y = position.y * speedRun;
            }
            // Walking ?
            else
            {
                // Apply walk
                vectorMove.y = position.y * speedWalk;
            }
        }
    }

    private void StartJumpY(bool button)
    {
        // Button is different ?
        if (button != primaryButtonY)
        {
            // Retain button state
            primaryButtonY = button;
            // Grounded and button pressed ? 
            if (characterController.isGrounded && primaryButtonY && !stateFly && canJump)
            {
                speedY += speedJump;
            }
        }
    }

    private void ApplyGravity()
    {
        // Update speedY for gravity
        speedY += Physics.gravity.y * Time.deltaTime;
        // Include speedY in move vector
        vectorMove.y = speedY;
    }

    private void SetMovementStateRun(bool value)
    {
        // Store new setting
        stateRun = value;
        // Raise event
        if (eventBoolStateRun != null) eventBoolStateRun.Invoke(stateRun);
    }

    private void SetMovementStateFly(bool value)
    {
        // Store new setting
        stateFly = value;
        // Raise event
        if (eventBoolStateFly != null) eventBoolStateFly.Invoke(stateFly);
    }
}
